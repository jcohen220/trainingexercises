# Jason Cohen
## New Hire Training Questions

**1)** How many Devices do we currently have?

* `1,539,182,256`

**2)** How many Devices added the last 30 days?  (Hint: check your answer against the Geckoboard)

* `2,788,331`

**3)** How many of these Devices have at least one persona?

* `42,122,163`

**4)** How many ~~total~~ Beacons have we detected **in the last 5 days**?

* `4,070,949`

**5)** How many Beacons have we detected over the last ~~30~~ 5 days?
Hoq many of these do not have the UUID `6CA0C73C-F8EC-4687-9112-41DCB6F28879 or 66622E6D-652F-40CA-9E6F-6F7166616365 or 5993A94C-7D97-4DF7-9ABF-E493BFD5D000` (use a case insensitive search) ? 

* `1,846,834`

**6)** How many Beacons do we have classified? (Hint: check your answer against the Geckoboard)

* `5604`

**7)** What cities have the most Classified Beacons?
```
214	Washington
108	San Antonio
99	Los Angeles
91	Atlanta
83	Lafayette
```


**8a)** What Businesses/Places have the most beacon bumps over the last 15 days?
```
27481	CVS Pharmacy
20023	Target
16168	Starbucks Coffee
12111	Walmart
```

**8b)** What Businesses/Places have the most unique devices over the last 15 days?
``` 	
 	num_devices	name
1	51919157	Las Vegas Strip + Hotels
2	20800766	Classic Car Show
3	17277368	CVS Pharmacy
4	16050095	Walgreens
5	13693994	McDonald's
```

**9)** What Business Chains have the most classified beacons? 
```
36607	Cardtronics ATM
17693	CVS Pharmacy
6339	Presto! ATM at Publix®
5433	Starbucks
```

**10)** Over the last 15 days, what is our average number of daily active devices? (active = device has an event that day)

* `39,740,113`

**11)** What are the top 5 apps over the last ~~30~~ 5 days which contributed the most new devices with a home location, work location, other?

* *Home*
```
1	10867933	UberMedia
2	2932082	ScanBuy
3	2037942	OneSignal
4	1900428	automatic-164C49252983F12B0A7282DFCF76B2C8
5	1416413	automatic-70F5E6C206B669EA9917FCE146E9A185
```

* *Work*
```
1	5539741	UberMedia
2	1630036	ScanBuy
3	1090633	automatic-164C49252983F12B0A7282DFCF76B2C8
4	719694	automatic-70F5E6C206B669EA9917FCE146E9A185
5	650635	automatic-320B4122BAD77E648A44F2C4F85A7182
```

**13)** How many uniques devices did we share with Qualia(data-partner) yesterday? 

* `Qualia	1,190,118	2017-11-21`


**14)** What are the different beacon types according to Reveal? (beacon_type field in "BeaconClass" table)
```
Static Beacon
Multi Beacon
Unclassified
Custom Beacon
```