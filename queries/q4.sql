/*
 * Source: Athena
 */
 
SELECT approx_distinct(concat(cast(beacon_uuid AS varchar),
         concat(cast(beacon_minor AS varchar),
         cast(beacon_major AS varchar))))
FROM event
WHERE year = 2018
        AND month = 12
        AND day IN (12, 11, 10, 9, 8)
        AND type_id = 2
