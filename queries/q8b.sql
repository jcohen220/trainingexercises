/**
 * Source Athena
 */
SELECT count(place_event.advertiser_id) AS num_devices,
         place.name
FROM place_event, place
WHERE place_event.place_id = place.id
        AND place_event.year = 2018
        AND place_event.month = 12
GROUP BY  place.name
ORDER BY  count(place_event.advertiser_id) desc;