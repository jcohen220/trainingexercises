/*
 * Source: Athena
 */
SELECT approx_distinct(advertiser_id) AS num_personas
FROM device_persona
WHERE created_on >= current_date - interval '30' day

