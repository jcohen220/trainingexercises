/** 
 * Source: Athena
 */

 -- work
with x AS 
    (SELECT a.name,
         (CASE
        WHEN d.work_lat is NOT null
            AND d.work_lon is NOT NULL THEN
        1
        ELSE 0 END) AS has_work_address
    FROM device d, daily_device_app dda, app a
    WHERE d.advertiser_id = dda.advertiser_id
            AND dda.app_id = a.id
            AND dda.year = 2018
            AND dda.month = 12
            AND dda.day in (12, 11, 10, 9, 8))
SELECT sum(x.has_work_address) AS sum_has_work,
         name
FROM x
GROUP BY  name
ORDER BY  sum(x.has_work_address) desc
LIMIT 5;

--home
with x AS 
    (SELECT a.name,
         (CASE
        WHEN d.lat_pr is NOT null
            AND d.lon_pr is NOT NULL THEN
        1
        ELSE 0 END) AS has_home_address
    FROM device d, daily_device_app dda, app a
    WHERE d.advertiser_id = dda.advertiser_id
            AND dda.app_id = a.id
            AND dda.year = 2018
            AND dda.month = 12
            AND dda.day in (12, 11, 10, 9, 8))
SELECT sum(x.has_home_address) AS sum_has_home,
         name
FROM x
GROUP BY  name
ORDER BY  sum(x.has_home_address) desc
LIMIT 5;
