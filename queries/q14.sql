/* 
 * Source: Nearkat
 */
SELECT DISTINCT beacon_type 
FROM "BeaconClass" 
WHERE beacon_type is not null;
