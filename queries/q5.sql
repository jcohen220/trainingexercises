/*
 * Source: Nearkat
 */
 
SELECT approx_distinct(concat(cast(beacon_uuid AS varchar),
         concat(cast(beacon_minor AS varchar),
         cast(beacon_major AS varchar))))
FROM event
WHERE year = 2018
        AND month = 12
        AND day IN (12, 11, 10, 9, 8)
        AND type_id = 2
        AND lower(beacon_uuid) NOT IN (
        lower('CA0C73C-F8EC-4687-9112-41DCB6F28879'),
        lower('66622E6D-652F-40CA-9E6F-6F7166616365'),
        lower('5993A94C-7D97-4DF7-9ABF-E493BFD5D000'))