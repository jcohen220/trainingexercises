/*
 * Source: Athena
 */
SELECT approx_distinct(advertiser_id)
FROM device
WHERE created_on >= current_date - interval '30' day;