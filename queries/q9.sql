/**
 * Source Nearkat
 */
select count(bc.beacon_id), pg.name
from "BeaconClass" bc, place_google pg
where bc.place_id = pg.id
group by pg.name
order by count(bc.beacon_id) desc;