/**
 * Source: Nearkat
 */

with x as
       (select job_name, device_count, day from prodfeed_invoice where job_name = 'Qualia' order by day desc)
select job_name, device_count, day
from x
where day = (select max(day) from x);