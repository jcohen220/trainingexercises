/**
 * Source: Athena
 */
SELECT cast(avg(count_ids) AS integer)
FROM 
    (SELECT cast(count(distinct(advertiser_id)) AS integer) AS count_ids
    FROM daily_device_stats
    WHERE year = 2018
            AND ( month = 12
            OR (month = 11
            AND day IN (29,30)) ) ) 