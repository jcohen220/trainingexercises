/**
 * Source Athena
 */
SELECT count(e.id) AS count_beacon_bumps,
         p.name
FROM event e, place_event pe, place p
WHERE e.id = pe.event_id
        AND pe.place_id = p.id 
        --place event date filter
        AND pe.year = 2018
        AND pe.month = 12
        AND pe.day in (12, 11, 10, 9, 8)
        --e event date filter
        AND e.year = 2018
        AND e.month = 12
        AND e.day in (12, 11, 10, 9, 8)
        AND e.type_id = 2
        AND e.beacon_uuid is NOT null
        AND trim(e.beacon_uuid) != ''
GROUP BY  p.name
ORDER BY  count(e.id) desc;