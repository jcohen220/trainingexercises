/**
 * Source Nearkat
 */

SELECT 
	count(bc.uuid) as count_beacons, 
	pg.city
FROM 
	"BeaconClass" bc, place_google pg
WHERE 
	bc.place_id = pg.id
	and pg.city is not null
	and trim(pg.city) != ''
GROUP BY 
	pg.city
ORDER BY 
	count_beacons desc;
